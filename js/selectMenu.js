"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// bạn có thể dùng để lưu trữ combo được chọn,
// mỗi khi khách chọn menu S, M, L bạn lại đổi giá trị properties của nó
var gOrderDB = {
    OrderObj: {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
    },
    DrinkObj: [],
};
var gOrderInfor = {};
// bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// load trang
onPageLoading();

$("#sizeS").on("click", function() {
    onBtnSizeSClick();
});
$("#sizeM").on("click", function() {
    onBtnSizeMClick();
});
$("#sizeL").on("click", function() {
    onBtnSizeLClick();
});
$("#typeM").on("click", function() {
    onBtnTypeMClick();
});
$("#typeC").on("click", function() {
    onBtnTypeCClick();
});
$("#typeS").on("click", function() {
    onBtnTypeSClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện load trang
function onPageLoading() {
    onLoadDrinkListFromApi();
}
//selected the size M
function onBtnSizeMClick() {
    "use strict";
    var vSize = "M";
    //Gọi setmenu cho size
    setMenuSelect(vSize, gOrderDB.OrderObj);
}
//selected the size L
function onBtnSizeLClick() {
    "use strict";
    var vSize = "L";
    //Gọi setmenu cho size
    setMenuSelect(vSize, gOrderDB.OrderObj);
}
//selected the size S
function onBtnSizeSClick() {
    "use strict";
    var vSize = "S";
    //Gọi setmenu cho size
    setMenuSelect(vSize, gOrderDB.OrderObj);
}
//selected the type M
function onBtnTypeMClick() {
    // map lựa chọn vào gSelectedPizzaType
    var vSize = "M";
    //Gọi set pizza type cho size
    setPizzaTypeSelect(vSize, gOrderDB.OrderObj);
}
//selected the type M
function onBtnTypeCClick() {
    // map lựa chọn vào gSelectedPizzaType
    var vSize = "L";
    //Gọi set pizza type cho size
    setPizzaTypeSelect(vSize, gOrderDB.OrderObj);
}

function onBtnTypeSClick() {
    // map lựa chọn vào gSelectedPizzaType
    var vSize = "S";
    //Gọi set pizza type cho size
    setPizzaTypeSelect(vSize, gOrderDB.OrderObj);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Hàm select menu pizza
function setMenuSelect(paramMenu, paramOrderObj) {
    switch (paramMenu) {
        case "M":
            // map lựa chọn vào gSelectedMenuStructure
            paramOrderObj.kichCo = "M";
            //Gán giá trị cho menu được chọn
            paramOrderObj.duongKinh = 25;
            paramOrderObj.suon = 4;
            paramOrderObj.salad = 300;
            paramOrderObj.soLuongNuoc = 3;
            paramOrderObj.thanhTien = 200000;
            break;
        case "L":
            // map lựa chọn vào gSelectedMenuStructure
            paramOrderObj.kichCo = "L";
            //Gán giá trị cho menu được chọn
            paramOrderObj.duongKinh = 30;
            paramOrderObj.suon = 8;
            paramOrderObj.salad = 500;
            paramOrderObj.soLuongNuoc = 4;
            paramOrderObj.thanhTien = 250000;
            break;
        default:
            // map lựa chọn vào gSelectedMenuStructure
            paramOrderObj.kichCo = "S";
            //Gán giá trị cho menu được chọn
            paramOrderObj.duongKinh = 20;
            paramOrderObj.suon = 2;
            paramOrderObj.salad = 200;
            paramOrderObj.soLuongNuoc = 2;
            paramOrderObj.thanhTien = 150000;
    }
    //set trạng thái của nut - set the buttons style
    setMenuButtonsColor(paramOrderObj.kichCo);
    //Ghi console của menu ra màn hình
    showMenuUserSelect(paramOrderObj);
}
//Hàm select Pizza Type
function setPizzaTypeSelect(paramMenu, paramOrderObj) {
    switch (paramMenu) {
        case "M":
            //Gán giá trị cho menu được chọn
            paramOrderObj.loaiPizza = "Seafood";
            break;
        case "L":
            //Gán giá trị cho menu được chọn
            paramOrderObj.loaiPizza = "Hawaii";
            break;
        default:
            //Gán giá trị cho menu được chọn
            paramOrderObj.loaiPizza = "Bacon";
    }
    //set trạng thái của nut - set the buttons style
    setTypePizzaButtonsColor(paramMenu);
    //Ghi console của trạng thái ra màn hình
    showMenuUserSelect(paramOrderObj);
}
//set trạng thái của 03 nút trong menu (tại price pannel)
function setMenuButtonsColor(paramSelectedMenuName) {
    "use strict";
    //truy vấn 03 nút
    console.log("selected menu .. = " + paramSelectedMenuName); //tracking
    var vSelectSizeMButton = $("#sizeM");
    var vSelectSizeLButton = $("#sizeL");
    var vSelectSizeSButton = $("#sizeS");
    // set classname cho 03 nút này, tùy và lựa chon là gì
    switch (paramSelectedMenuName) {
        case "M":
            vSelectSizeMButton.addClass("btn btn-warning text-white"); //oragge
            vSelectSizeLButton.removeClass("btn-warning text-white");
            vSelectSizeSButton.removeClass("btn-warning text-white");
            break;
        case "L":
            vSelectSizeMButton.removeClass("btn-warning text-white");
            vSelectSizeLButton.addClass("btn btn-warning text-white"); //orange
            vSelectSizeSButton.removeClass("btn-warning text-white");
            break;
        default:
            vSelectSizeMButton.removeClass("btn-warning text-white");
            vSelectSizeLButton.removeClass("btn-warning text-white");
            vSelectSizeSButton.addClass(" btn btn-warning text-white"); //orange
    }
}
//set trạng thái của 03 nút trong menu (tại price pannel)
function setTypePizzaButtonsColor(paramSelectedTypeName) {
    "use strict";
    //truy vấn 03 nút
    console.log("selected menu .. = " + paramSelectedTypeName); //tracking
    var vSelectTypeMButton = $("#typeM");
    var vSelectTypeCButton = $("#typeC");
    var vSelectTypeSButton = $("#typeS");
    // set classname cho 03 nút này, tùy và lựa chon là gì
    switch (paramSelectedTypeName) {
        case "M":
            vSelectTypeMButton.addClass(" btn btn-warning text-white"); //oragge
            vSelectTypeCButton.removeClass("btn-warning text-white");
            vSelectTypeSButton.removeClass("btn-warning text-white");
            break;
        case "L":
            vSelectTypeMButton.removeClass("btn-warning text-white");
            vSelectTypeCButton.addClass(" btn btn-warning text-white"); //orange
            vSelectTypeSButton.removeClass("btn-warning text-white");
            break;
        default:
            vSelectTypeMButton.removeClass("btn-warning text-white");
            vSelectTypeCButton.removeClass("btn-warning text-white");
            vSelectTypeSButton.addClass(" btn btn-warning text-white"); //orange
    }
}

function showMenuUserSelect(paramSeclectedMenuName) {
    "use strict";
    //Ghi console của menu được chọn ra màn hình
    console.log(paramSeclectedMenuName);
}
//Hàm load dữ liệu drink list từ API
function onLoadDrinkListFromApi() {
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
        type: "GET",
        dataType: "json",
        success: function(responseObject) {
            gOrderDB.DrinkObj = responseObject;
            console.log(gOrderDB.DrinkObj);
            loadDataToDrinkSelect(gOrderDB.DrinkObj);
        },
        error: function(error) {
            console.assert(error.responseText);
        },
    });
}
// load drink select
function loadDataToDrinkSelect(paramDataObj) {
    var vDrinkSelect = $("#select-drink");

    var vDrinkOption = $("<option/>", {
        value: "NONE",
        text: "--- Tất cả loại nước uống ---",
    }).appendTo(vDrinkSelect);

    for (var bI = 0; bI < paramDataObj.length; bI++) {
        var vDrinkOption = $("<option/>", {
            value: paramDataObj[bI].maNuocUong,
            text: paramDataObj[bI].tenNuocUong,
        }).appendTo(vDrinkSelect);
    }
    //add eventlistenter; nếu có value mới thì phải đổ lại dữ liệu
    vDrinkSelect.change(function() {
        onSelectDrinkValueChange(this);
    });
}
//On select drink change
function onSelectDrinkValueChange(paramSelectDrink) {
    "user strict";
    gOrderDB.OrderObj.idLoaiNuocUong = paramSelectDrink.value;
    /*   gDrink.drinkName = paramSelectDrink.options[paramSelectDrink.selectedIndex].text;; */
}