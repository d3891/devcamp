"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gSELECT_NULL_VALUE = "NONE";
const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
//Send order click
$("#btn-send-data").on("click", function () {
  onBtnSendDataClick();
});
//Tạo đơn hàng btn-submit
$("#btn-submit").on("click", function () {
    onBtnSendOrderClick();
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onBtnSendDataClick() {
  console.log("Gửi dữ liệu đặt hàng");
  //debugger;
  // B1: thu thập dữ liệu
  getFormData(gOrderDB.OrderObj);
  // B2: kiểm tra dữ liệu
  var vIsValidData = validateData(gOrderDB.OrderObj);

  if (vIsValidData) {
    // B3: ghi dữ liệu vào vùng màu vàng
    showDataUser(gOrderDB.OrderObj);
  }
}
function onBtnSendOrderClick() {
    //debugger;
    // B1: Tiến hành tạo order
    sendRequestCreateOrder(gOrderDB.OrderObj);
    // B2: Thành công thì xóa dữ liệu order
    resetData();
  
    //Ản modal hiện tại
    $("#order-modal").modal("hide");
  }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//Create from api
function sendRequestCreateOrder(paramOrderObject) {
  "use strict";
  var vObjectRequest = JSON.stringify(paramOrderObject);
  console.log(vObjectRequest);
  // phải implement ajax put call tại đây
  $.ajax({
    url: gBASE_URL,
    type: "POST",
    dataType: "json",
    contentType: "application/json;charset=UTF-8",
    data: vObjectRequest,
    success: function (res) {
        swal({
            title: "Thêm đơn hàng đã thành công ",
            text: "Mã đơn đặt hàng của bạn là" + res.orderId,
            icon: "success",
            button: "Xác nhận!",
          });
      console.log(res);
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    },
  });
}

function getFormData(paramUser) {
  //truy suất các element trên form
  var vInputFullNameElement = $("#inp-fullname");
  var vInputEmailElement = $("#inp-email");
  var vInputFoneNumberElement = $("#inp-dien-thoai");
  var vInputAddressElement = $("#inp-dia-chi");
  var vInputMessageElement = $("#inp-message");
  var vInputDiscountElement = $("#inp-discount");
  //gán giá trị cho đối tượng người
  paramUser.hoTen = vInputFullNameElement.val().trim();
  paramUser.email = vInputEmailElement.val().trim();
  paramUser.soDienThoai = vInputFoneNumberElement.val().trim();
  paramUser.diaChi = vInputAddressElement.val().trim();
  paramUser.loiNhan = vInputMessageElement.val().trim();
  paramUser.idVourcher = vInputDiscountElement.val().trim();
}
// hàm validate dữ liệu đầu vào
function validateData(paramOrderObj) {
  //truy xuất các validate
  var vResult = true;
  //truy suất các element trên form
  var vInputFullNameElement = $("#inp-fullname");
  var vInputEmailElement = $("#inp-email");
  var vInputFoneNumberElement = $("#inp-dien-thoai");
  var vInputAddressElement = $("#inp-dia-chi");
  var vValidateFullNameElement = $("#val-fullname");
  var vValidateEmailElement = $("#val-email");
  var vValidateFoneNumberElement = $("#val-fonenumber");
  var vValidateAddressElement = $("#val-address");
  if (
    paramOrderObj.kichCo === gSELECT_NULL_VALUE ||
    paramOrderObj.kichCo === ""
  ) {
    swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một kiểu Pizza!",
        icon: "warning",
        button: "Xác nhận!",
      });
    vResult = false;
    throw "Bạn cần chọn một kiểu Pizza!";
  }
  if (
    paramOrderObj.idLoaiNuocUong === gSELECT_NULL_VALUE ||
    paramOrderObj.idLoaiNuocUong === ""
  ) {
    swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một loại đồ uống!",
        icon: "warning",
        button: "Xác nhận!",
      });
    vResult = false;
    throw "Bạn cần chọn một loại đồ uống!";
  }
  if (
    paramOrderObj.loaiPizza === gSELECT_NULL_VALUE ||
    paramOrderObj.loaiPizza === ""
  ) {
    swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một loại Pizza!",
        icon: "warning",
        button: "Xác nhận!",
      }); 
    vResult = false;
    throw "Bạn cần chọn một loại Pizza!";
  }
  if (paramOrderObj.hoTen === "") {
    swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đầy đủ họ và tên!",
        icon: "warning",
        button: "Xác nhận!",
      }); 
    //Remove các cảnh báo trong form
    vResult = false;
    vInputFullNameElement.trigger("focus");
    throw "Vui lòng nhập đủ họ tên";
  }
  if (!validateEmail(paramOrderObj.email)) {
    swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đúng địa chỉ email!",
        icon: "warning",
        button: "Xác nhận!",
      }); 
    vResult = false;
    vInputEmailElement.trigger("focus");
    throw "Bạn cần nhập đúng địa chỉ email!";
  }
  if (!validatePhoneNumber(paramOrderObj.soDienThoai)) {
    //Remove các cảnh báo trong form
    swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đúng số điện thoại!",
        icon: "warning",
        button: "Xác nhận!",
      }); 
    vResult = false;
    vInputFoneNumberElement.trigger("focus");
    throw "Bạn cần nhập đúng số điện thoại!";
  }
  if (paramOrderObj.diaChi === "") {
    //Remove các cảnh báo trong form
    swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đầy đủ địa chỉ!!",
        icon: "warning",
        button: "Xác nhận!",
      }); 
    vResult = false;
    vInputAddressElement.trigger("focus");
    throw "Bạn cần nhập đầy đủ địa chỉ!";
  }

  return vResult;
}
//Hàm validate email
function validateEmail(paramEmail) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(paramEmail).toLowerCase());
}

//Hàm Validate phone numbet
function validatePhoneNumber(paramPhoneNumber) {
  var vResult = true;
  var vPhoneNumber = /((09|03|07|08|05)+([0-9]{8})\b)/g;
  if (vPhoneNumber.test(paramPhoneNumber) === true) {
    vResult = true;
  } else {
    vResult = false;
  }
  return vResult;
}
//Hàm hiển thị dữ liệu người dùng
function showDataUser(paramOrderObj) {
  //truy suất các element trên form
  var vInputFullNameElement = $("#inp-modal-fullname");
  var vInputEmailElement = $("#inp-modal-email");
  var vInputFoneNumberElement = $("#inp-modal-dien-thoai");
  var vInputAddressElement = $("#inp-modal-dia-chi");
  var vInputMessageElement = $("#inp-modal-message");
  var vInputDiscountElement = $("#inp-modal-discount");
  var vInputInforElement = $("#inp-modal-infor");
  var vDivSubjectInforElement = "";
  var vShowVoucher = "";
  var vVoucher = parseInt(
    sendRequestCheckVoucherCode(paramOrderObj.idVourcher)
  );
  // hàm này trả về một đối tượng

  var vSumPrice = "";

  console.log("Voucer" + vVoucher);

  //gán giá trị người đặt hàng
  vInputFullNameElement.val(paramOrderObj.hoTen);
  vInputEmailElement.val(paramOrderObj.email);
  vInputFoneNumberElement.val(paramOrderObj.soDienThoai);
  vInputAddressElement.val(paramOrderObj.diaChi);
  vInputMessageElement.val(paramOrderObj.loiNhan);
  vInputDiscountElement.val(paramOrderObj.idVourcher);
  if (vVoucher === null || isNaN(vVoucher)) {
    vDivSubjectInforElement = "Mã giảm giá không tồn tại ";
    vShowVoucher = "";
    vSumPrice = paramOrderObj.thanhTien + "VND";
  } else {
    vShowVoucher = ", Mã giảm giá: " + paramOrderObj.idVourcher;
    vSumPrice = (paramOrderObj.thanhTien * (100 - vVoucher)) / 100;
    vSumPrice += "VND ,(Giảm giá" + vVoucher + "%)";
  }
  vDivSubjectInforElement =
    "Xác nhận: " +
    paramOrderObj.hoTen +
    ", " +
    paramOrderObj.soDienThoai +
    ", " +
    paramOrderObj.diaChi +
    "\nMenu: " +
    paramOrderObj.kichCo +
    ", sườn nướng: " +
    paramOrderObj.suon +
    ", nước: " +
    paramOrderObj.soLuongNuoc +
    "...\n" +
    "Loại Pizza: " +
    paramOrderObj.loaiPizza +
    ", Giá: " +
    paramOrderObj.thanhTien +
    "VND " +
    vShowVoucher +
    "\nPhải thanh toán: " +
    vSumPrice;
  console.log(vDivSubjectInforElement);
  vInputInforElement.val(vDivSubjectInforElement);

  //Hiển thị modal lên
  $("#order-modal").modal("show");
}

function sendRequestCheckVoucherCode(paramVoucherId) {
  var vResponse = null;
  if (paramVoucherId === "") {
    return null;
  } else {
    $.ajax({
      url:
        "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" +
        paramVoucherId,
      type: "GET",
      dataType: "json",
      async: false,
      success: function (responseObject) {
        vResponse = responseObject;
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
    return vResponse.phanTramGiamGia;
  }
}
function resetData() {
    var vInputFullNameElement = $("#inp-fullname");
    var vInputEmailElement = $("#inp-email");
    var vInputFoneNumberElement = $("#inp-dien-thoai");
    var vInputAddressElement = $("#inp-dia-chi");
    var vInputMessageElement = $("#inp-message");
    var vInputDiscountElement = $("#inp-discount");
    vInputFullNameElement.val("");
    vInputEmailElement.val("");
    vInputFoneNumberElement.val("");
    vInputAddressElement.val("");
    vInputMessageElement.val("");
    vInputDiscountElement.val("");
    gOrderDB.OrderObj={};
}
