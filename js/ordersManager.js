"use strict";
$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
  const gSELECT_NULL_VALUE = "NONE";
  const gORDER_ID_COL = 0;
  const gKICH_CO_COL = 1;
  const gLOAI_PIZZA_COL = 2;
  const gNUOC_UONG_COL = 3;
  const gTHANH_TIEN_COL = 4;
  const gHOTEN_COL = 5;
  const gSO_DIEN_THOAI_COL = 6;
  const gTRANG_THAI_COL = 7;
  const gACTION_COL = 8;

  const gORDER_COL = [
    "orderId",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "thanhTien",
    "hoTen",
    "soDienThoai",
    "trangThai",
    "action",
  ];
  // Khai báo đối tượng chứa dữ liệu trên form
  var gOrderObj = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
    trangThai: "",
  };
  //Khai báo các thuộc tính trong modal
  var gOrderIdElement = $("#inp-orderId");
  var gSizeComboSelectElement = $("#select-combo");
  var gPizzaKindElement = $("#inp-PizzaKind");
  var gMeatNumElement = $("#inp-MeatNum");
  var gDrinkSelectElement = $("#select-drink");
  var gNumberDrinkElement = $("#inp-NumberDrink");
  var gVoucherIdElement = $("#inp-VoucherId");
  var gPizzaTypeElement = $("#inp-PizzaType");
  var gSaladElement = $("#inp-Salad");
  var gThanhTienElement = $("#inp-ThanhTien");
  var gGiamGiaElement = $("#inp-GiamGia");
  var gFullNameElement = $("#inp-FullName");
  var gEmailElement = $("#inp-email");
  var gPhoneNumberElement = $("#inp-PhoneNumber");
  var gDiaChiElement = $("#inp-DiaChi");
  var gLoiNhanElement = $("#inp-LoiNhan");
  var gOrderStatusSelectElement = $("#select-status");
  var gNgayTaoDonElement = $("#inp-NgayTaoDon");
  var gNgayCapNhatElement = $("#inp-NgayCapNhat");
  //Khai báo order DB
  var gOrderDb = {
    orders: [],
    // các phương thức để làm việc với dữ liệu  orders
    filterOrder: function (paramDataFilter) {
      var vOrderResult = [];
      vOrderResult = this.orders.filter(function (order) {
        if (order.trangThai != null && order.loaiPizza != null) {
          return (
            (order.trangThai.toLowerCase() ===
              paramDataFilter.trangThai.toLowerCase() ||
              paramDataFilter.trangThai === "") &&
            (order.loaiPizza.toLowerCase() ===
              paramDataFilter.loaiPizza.toLowerCase() ||
              paramDataFilter.loaiPizza === "")
          );
        }
      });
      return vOrderResult;
    },
  };
  var vOrderIdObj = {
    Id: "",
    OrderId: "",
  };
  // định nghĩa table  - chưa có data
  var gOrderTable = $("#Order-table").DataTable({
    // Khai báo các cột của datatable
    columns: [
      {
        data: gORDER_COL[gORDER_ID_COL],
      },
      {
        data: gORDER_COL[gKICH_CO_COL],
      },
      {
        data: gORDER_COL[gLOAI_PIZZA_COL],
      },
      {
        data: gORDER_COL[gNUOC_UONG_COL],
      },
      {
        data: gORDER_COL[gTHANH_TIEN_COL],
      },
      {
        data: gORDER_COL[gHOTEN_COL],
      },
      {
        data: gORDER_COL[gSO_DIEN_THOAI_COL],
      },
      {
        data: gORDER_COL[gTRANG_THAI_COL],
      },
      {
        data: gORDER_COL[gACTION_COL],
      },
    ],
    // Ghi đè nội dung của cột action, chuyển thành button chi tiết
    columnDefs: [
      {
        targets: gACTION_COL,
        className: "text-center",
        defaultContent: `
      <button class='info-order btn btn-info'>Chi tiết</button>`,
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  // gán click event handler cho button chi tiet
  $("#Order-table").on("click", ".info-order", function () {
    onButtonChiTietClick(this); // this là button được ấn
  });

  $("#btn-filter-order").on("click", function () {
    onBtnFilterUserClick();
  });
  $("#btn-submit").on("click", function () {
    onBtnSubmitOrderDataClick();
  });
  $("#btn-cancel").on("click", function () {
    onBtnCallBackPageClick();
  });
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // infoFunction sẽ là function các nút cùng gọi
  function onButtonChiTietClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents("tr");
    //Lấy datatable row
    var vDatatableRow = gOrderTable.row(vRowSelected);
    //Lấy data của dòng
    var vOrderData = vDatatableRow.data();
    var vId = vOrderData.id;
    var vOrderId = vOrderData.orderId;
    var vKichCo = vOrderData.kichCo;
    var vLoaiPizza = vOrderData.loaiPizza;
    var vLoaiNuocUong = vOrderData.idLoaiNuocUong;
    var vThanhTien = vOrderData.thanhTien;
    var vHoTen = vOrderData.hoTen;
    var vSoDienThoai = vOrderData.soDienThoai;
    var vTrangThai = vOrderData.trangThai;
    console.log(vId);
    console.log(vOrderId);
    console.log(vKichCo);
    console.log(vLoaiPizza);
    console.log(vLoaiNuocUong);
    console.log(vThanhTien);
    console.log(vHoTen);
    console.log(vSoDienThoai);
    console.log(vTrangThai);
    vOrderIdObj.Id = vId;
    vOrderIdObj.OrderId = vOrderId;
    console.log("Id: " + vId);
    console.log("Order Id: " + vOrderId);
    getOrderById(vOrderIdObj.OrderId);
    $("#order-modal").modal("show");
  }

  // hàm chạy khi trang được load
  function onPageLoading() {
    "use strict";
    // lấy data từ server
    onLoadOrderListFromApi();

    loadDataToDrinkSelectFromApi();
  }

  // hàm xử lý sự kiện filter users
  function onBtnFilterUserClick() {
    // Khai báo đối tượng chứa dữ liệu lọc trên form
    var vOrderFilterDataObj = {
      trangThai: "",
      loaiPizza: "",
    };
    // B1: Thu thập dữ liệu
    getFilterData(vOrderFilterDataObj);
    // B2: Validate (ko cần)
    // B3: Thực hiện nghiệp vụ lọc
    var vOrderFilterResult = gOrderDb.filterOrder(vOrderFilterDataObj);
    // B4: Hiển thị dữ liệu lên table
    loadDataToTable(vOrderFilterResult);
  }

  function onBtnSubmitOrderDataClick() {
    "use strict";
    console.log("Submit Order Data Click");
    // Bước 0: Khai báo đối tượng

    // Bước 1: thu thập dữ liệu
    setDataOrder(gOrderObj);
    // Bước 2: Validate
    var vCheck = validateData(gOrderObj);
    if (vCheck) {
      console.log(gOrderObj);
      callAjaxRestApiUpdate(gOrderObj.orderId, gOrderObj);
      $("#order-modal").modal("hide");
      //reset dữ liệu
      resetData();
    }
  }
  // Hàm thực hiện khi nút call back được click
  function onBtnCallBackPageClick() {
    console.log("Id" + vOrderIdObj.Id);
    gOrderObj.trangThai = "cancel";
    callAjaxRestApiUpdate(vOrderIdObj.Id, gOrderObj);
    //reset dữ liệu
    resetData();
    $("#order-modal").modal("hide");
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  //Hàm load dữ liệu order list từ API
  function onLoadOrderListFromApi() {
    $.ajax({
      url: gBASE_URL,
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        gOrderDb.orders = responseObject;
        console.log(gOrderDb.orders);
        loadDataToTable(gOrderDb.orders);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }
  // load data to table
  function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderTable.clear();
    //Cập nhật data cho bảng
    gOrderTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderTable.draw();
  }

  // hàm thu thập dữ liệu lọc trên form
  function getFilterData(paramFilterObj) {
    paramFilterObj.trangThai = $("#select-trangThai").val();
    paramFilterObj.loaiPizza = $("#select-loaiPizza").val();
  }
  // hàm load dữ liệu vào select drink
  function loadDataToDrinkSelectFromApi() {
    // B1: thu thập dữ liêu (ko có)
    // B2: validate dữ liệu (ko có)
    // B3: gọi request server để lấy dữ liệu drinks
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
      type: "GET",
      dataType: "json", // added data type
      success: function (res) {
        console.log(res);
        onShowDrinkOnSelect(res);
      },
      error: function (ajaxContext) {
        alert(ajaxContext.responseText);
      },
    });
  }

  function onShowDrinkOnSelect(paramSelect) {
    var vDrinkObjs = paramSelect;
    var vDrinkSelect = $("#select-drink");
    for (let bI = 0; bI < vDrinkObjs.length; bI++) {
      vDrinkSelect.append(
        $("<option>")
          .attr("value", vDrinkObjs[bI].maNuocUong.toLowerCase())
          .text(vDrinkObjs[bI].tenNuocUong)
      );
    }
  }

  function getOrderById(paramId) {
    "use strict";
    //Khai bao bien va gan gia tri cho Id
    var vOrderId = paramId;
    $.ajax({
      url: gBASE_URL + "/" + vOrderId,
      type: "GET",
      dataType: "json", // added data type
      success: function (res) {
        console.log(res);
        readOrderDataToForm(res);
      },
      error: function (ajaxContext) {
        alert(ajaxContext.responseText);
      },
    });
  }
  //Update from api
  function callAjaxRestApiUpdate(paramId, paramOrderObject) {
    "use strict";
    var vOrderId = paramId;
    var vObjectRequest = JSON.stringify(paramOrderObject);
    // phải implement ajax put call tại đây
    $.ajax({
      url: gBASE_URL + "/" + vOrderId,
      type: "PUT",
      dataType: "json",
      contentType: "application/json;charset=UTF-8",
      data: vObjectRequest,
      success: function (res) {
        swal({
          title: "Cập nhật đơn hàng",
          text: "Cập nhật thông tin thành công: " + vOrderId,
          icon: "success",
          button: "Xác nhận!",
        });
        console.log(res);

        onLoadOrderListFromApi();
      },
      error: function (ajaxContext) {
        alert(ajaxContext.responseText);
      },
    });
  }
  //Đọc dữ liệu order từ API ra Form
  function readOrderDataToForm(paramObjectOrder) {
    //{"id":12851,"orderId":"4Y9QClNS1P","kichCo":"S","duongKinh":"20","suon":2,"salad":"200","loaiPizza":"Seafood","idVourcher":"95531","thanhTien":150000,"giamGia":15000,"idLoaiNuocUong":"COCA","soLuongNuoc":2,"hoTen":"Cuong","email":"hmceit2014@gmail.com","soDienThoai":"0934939996","diaChi":"An Ninh","loiNhan":"Chao ban","trangThai":"open","ngayTao":1635915539410,"ngayCapNhat":1635915539410}
    var vOrderDataObj = paramObjectOrder;
    gSizeComboSelectElement.val(vOrderDataObj.kichCo.toLowerCase());
    gDrinkSelectElement.val(vOrderDataObj.idLoaiNuocUong.toLowerCase());
    gOrderStatusSelectElement.val(vOrderDataObj.trangThai.toLowerCase());
    gOrderIdElement.val(vOrderDataObj.id);
    /* gOrderId = vOrderIdObj.OrderId; */
    gPizzaKindElement.val(vOrderDataObj.duongKinh);
    gMeatNumElement.val(vOrderDataObj.suon);
    gNumberDrinkElement.val(vOrderDataObj.soLuongNuoc);
    gVoucherIdElement.val(vOrderDataObj.idVourcher);
    gPizzaTypeElement.val(vOrderDataObj.loaiPizza);
    gSaladElement.val(vOrderDataObj.salad);
    gThanhTienElement.val(vOrderDataObj.thanhTien);
    gGiamGiaElement.val(vOrderDataObj.giamGia);
    gFullNameElement.val(vOrderDataObj.hoTen);
    gEmailElement.val(vOrderDataObj.email);
    gPhoneNumberElement.val(vOrderDataObj.soDienThoai);
    gDiaChiElement.val(vOrderDataObj.diaChi);
    gLoiNhanElement.val(vOrderDataObj.loiNhan);
    gNgayTaoDonElement.val(vOrderDataObj.ngayTao);
    gNgayCapNhatElement.val(vOrderDataObj.ngayCapNhat);
  }
  //Gán giá trị cho Order
  function setDataOrder(paramObjectOrder) {
    paramObjectOrder.orderId = gOrderIdElement.val().trim();
    paramObjectOrder.kichCo = gSizeComboSelectElement.val();
    paramObjectOrder.duongKinh = gPizzaKindElement.val().trim();
    paramObjectOrder.suon = gMeatNumElement.val().trim();
    paramObjectOrder.salad = gSaladElement.val().trim();
    paramObjectOrder.loaiPizza = gPizzaTypeElement.val().trim();
    paramObjectOrder.idVourcher = gVoucherIdElement.val().trim();
    paramObjectOrder.idLoaiNuocUong = gDrinkSelectElement.val();
    paramObjectOrder.soLuongNuoc = gNumberDrinkElement.val().trim();
    paramObjectOrder.hoTen = gFullNameElement.val().trim();
    paramObjectOrder.thanhTien = gThanhTienElement.val().trim();
    paramObjectOrder.email = gEmailElement.val().trim();
    paramObjectOrder.soDienThoai = gPhoneNumberElement.val().trim();
    paramObjectOrder.diaChi = gDiaChiElement.val().trim();
    paramObjectOrder.trangThai = gOrderStatusSelectElement.val();
    console.log("Order Id " + paramObjectOrder.orderId);
  }
  //hàm reset dữ liệu
  function resetData() {
    gSizeComboSelectElement.val("NONE");
    gDrinkSelectElement.val("NONE");
    gOrderStatusSelectElement.val("NONE");
    gOrderIdElement.val("");
    gPizzaKindElement.val("");
    gMeatNumElement.val("");
    gNumberDrinkElement.val("");
    gVoucherIdElement.val("");
    gPizzaTypeElement.val("");
    gSaladElement.val("");
    gThanhTienElement.val("");
    gGiamGiaElement.val("");
    gFullNameElement.val("");
    gEmailElement.val("");
    gPhoneNumberElement.val("");
    gDiaChiElement.val("");
    gLoiNhanElement.val("");
    gNgayTaoDonElement.val("");
    gNgayCapNhatElement.val("");
  }
  // hàm validate dữ liệu đầu vào
  function validateData(paramOrderObj) {
    var vResult = true;
    if (paramOrderObj.kichCo === gSELECT_NULL_VALUE) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một kiểu Pizza!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gSizeComboSelectElement.trigger("focus");
      throw "Bạn cần chọn một kiểu Pizza!";
      vResult = false;
    }
    if (paramOrderObj.loaiPizza === gSELECT_NULL_VALUE) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một loại Pizza!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gPizzaTypeElement.trigger("focus");
      throw "Bạn cần chọn một loại Pizza!";
      vResult = false;
    }
    if (paramOrderObj.idLoaiNuocUong === gSELECT_NULL_VALUE) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần chọn một loại đồ uống!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gDrinkSelectElement.trigger("focus");
      throw "Bạn cần chọn một loại đồ uống!";
      vResult = false;
    }
    if (paramOrderObj.hoTen === "") {
      swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đầy đủ họ và tên!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gFullNameElement.trigger("focus");
      throw "Bạn cần nhập đầy đủ họ và tên!";
      vResult = false;
    }
    if (!validateEmail(paramOrderObj.email)) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đúng địa chỉ email!!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gEmailElement.trigger("focus");
      throw "Bạn cần nhập đúng địa chỉ email!";
      vResult = false;
    }
    if (!validatePhoneNumber(paramOrderObj.soDienThoai)) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đúng số điện thoại!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gPhoneNumberElement.trigger("focus");
      throw "Bạn cần nhập đúng số điện thoại!";
      vResult = false;
    }
    if (paramOrderObj.diaChi === "") {
      swal({
        title: "Lỗi!",
        text: "Bạn cần nhập đầy đủ địa chỉ!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gDiaChiElement.trigger("focus");
      throw "Bạn cần nhập đầy đủ địa chỉ!";
      vResult = false;
    }
    if (paramOrderObj.trangThai === gSELECT_NULL_VALUE) {
      swal({
        title: "Lỗi!",
        text: "Bạn cần xác nhận trạng thái đơn hàng!",
        icon: "warning",
        button: "Xác nhận!",
      });
      gOrderStatusSelectElement.trigger("focus");
      throw "Bạn cần xác nhận trạng thái đơn hàng!!";
      vResult = false;
    }

    return vResult;
  }

  //Hàm validate email
  function validateEmail(paramEmail) {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(paramEmail).toLowerCase());
  }

  //Hàm Validate phone numbet
  function validatePhoneNumber(paramPhoneNumber) {
    var vPhoneNumber = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    if (vPhoneNumber.test(paramPhoneNumber) === true) {
      return true;
    } else {
      alert("Số điện thoại của bạn không đúng định dạng!");
      return false;
    }
  }
});
